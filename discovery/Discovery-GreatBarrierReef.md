# Architecture - Discovery  - Great Barrier Reef Dashboard

## Functionality
#### What frequency does the Dashboards need to present information in  

- Real Time
- Short Interval
- Reporting (Daily)

#### Can/Should end users be able to customize views presented?

Users get access to standard reports
User should be able choose metrices, filter query to get to information they may uniquely use. 

#### Can/Should end users be able to develop their own dashboards?

Should users be able to create their own dashboards/heads up displays that capture a number of views they need to see regularly.

#### Can users import/export their own data

Can users manually provide data?

#### Can users modify data

Can users update data which is incorrect.

Can users define their own derived columns?

#### Can users define their own models

Can/should users be able to define and uploads processing models to support their own research / reporting outcomes.

#### Can users define alerts and corresponding notifications to be alerted when particular simmple or complex events occur?

Is there some capability required to allow users to provide expressions which can transformed data into high level detected events?

## Data Sources
#### What technology solution is the source information accessible from
- spreadsheets
- sql databases
- no sql databases
- web apis
- stream feeds

#### Is there an canonical information model that all data needs to be mapped through

- Canonical Information Model - a large centralized informaiton model intended to describe, categorize and allow discovery of most data 
- Point solution/Pipeline specific models

#### What security measures are required to be established and maintained to get access to data sources.

- Back end integration

## Delivery
#### Can the solution be delivered in an incremental approach

Is it feasible to deliver some reports and then add to this list and so on?

#### What environments are currently provisioned and in use

- Training
- Development
- QA
- UAT
- Production

#### What components will be changing during steady state
- Data from data sources
- View configuration
- Users
- Authorization Rules

## Quality Concerns
#### What confidence is required that the dashboard presents reliable information as changed is delivered.

The question is really trying to understand large the QA effort should be as the system is evolved.
Potentially it is necessary to have an additional environment where beta testers use the solution before it is more widely release.

## Training / Acquisition

#### How will users get access?

#### How do users understand how to use the system effectively

- organized training
- self service training
- support desk

## Security
#### Which users will access the solution and what options are there to authenticate and authorization these users

- Solution specific identity provider
- Authorization Server to integrate multiple identity providers maintaining users in specific organizations.

#### What authorization model is required to access dashboards

What is the most fine grained authorization level required to control access
- the organization level
- the user level
- the data source level
- the data record level

#### Are there any privacy concerns with storage/processing of the data 

Does any data anonymization have to occur?
Is there any Personally Identifiable Information in the data being processed/incorporated in the solution


## Audit
#### Is an view audit trail required for the solution

The audit trail could record 
- which users 
- have acessed which views
- and which data queries 

#### Is an write/update audit trail required for the solution

The audit trail could record 
- which users 
- have changed configuration or business logic queries

## Landscape 
#### Does the information need to be available to other reporting engines

- Tableau
- Microsoft BI
- Web Hooks
- API Clients

#### What architecture/delivery/operations/support standards does the solution need to confirm to?

Are there any specific 

## Other Non Functionals

#### How many users are expected?

What volume of users is expected?

#### What response time is targetted to the end user?

- Sub Second
- Best efforts

#### What availability targets are there for the solution

- 99%
- 99.99 %

#### Does the system need to be available 24x7?

- 5 days x 8 hours per day
- 7 days x 10 hours per day
- 24 x 7

#### What are the expected criteria that the solution is up and running successfully?

- Synthentic transactions are all reporting success
- Core components are reporting healthy status checks

#### Is the system expected to provide some value even if not all subsystems are working successfully

Is the solution a number of cooperating system which may be different users/usage?

#### Should usage of the system be logged

Are there stake holders who would see benefit in getting access to who is using the system and how?

## Support

Is there a team identified to operate and support the solution?

#### What volume of data will be processed by the system on a daily basis?



## Programme

What design decisions are already been committed to?

What technology choices have already been committed to?


## Collateral

Are there any documents, diagrams  or other collateral that describes the intended system or subsystems

Are there documents which describe perspectives of the solution including
- security
- information model
- use cases
- data sources
- ui mockups


